argcomplete>=3.2.2
black>=24.2.0
httpx[http2]>=0.25.0
Jinja2==3.1.2
jsonschema>=4.19.2
json-schema-for-humans==0.41.8
linkchecker>=10.1.0
lxml>=5.2.1
markdown>=3.3.6
pandas>=2.2.0
progressbar2>=4.2.0
pyyaml>=6.0.1
requests>=2.31.0
tqdm>=0.0.0
Werkzeug>=3.1.3
yq>=3.1.0
